__author__ = 'abulaybulay'

import datetime

from email.mime.text import MIMEText
from smtplib import SMTP
import json
import os


settings_file = 'settings.json'
recipients_file = 'recipients.json'


def settings(json_file):
    """Returns iterable json data from a json file."""
    if os.path.isfile(json_file):
        json_settings = open(json_file)
        json_content = json_settings.read()
        settings_content = json.loads(json_content)
        return settings_content
    else:
        print('No settings file found')
        raw_input('Hit me to stop')


def get_recipients(journal_title):
    recipients_config = settings(recipients_file)
    recipients_name = recipients_config['recipients'][journal_title]['name']
    recipients_email = recipients_config['recipients'][journal_title]['email']
    return recipients_name, recipients_email


class SendEmail:
    def __init__(self):
        pass

    def send_mail(self, subject, message, journal):
        config = settings(settings_file)
        recipients_name, recipients = get_recipients(journal)
        all_recipients = recipients + config['email']['recipients']
        to_recipients = ", ".join(recipients)

        try:
            msg = MIMEText(message)
            msg['To'] = to_recipients
            msg['Cc'] = config['email']['cc']
            msg['From'] = config['email']['from']
            msg['Subject'] = subject + ' Time: ' +str(datetime.datetime.now().time())
            sender = SMTP(config['email']['mail_server'])
            sender.sendmail(config['email']['from'], all_recipients, msg.as_string())
            sender.quit()
            return True
        except Exception as e:
            print(e)
            return False


if __name__ == '__main__':
    mail = SendEmail()
    mail.send_mail(subject='DropboxUploader - Test Message',
    message='This is a test message from DropboxUploader', journal='ARTLSS')
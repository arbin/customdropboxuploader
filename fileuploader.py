__author__ = 'abulaybulay'
__about__ = 'This fileuploader uploads file to dropbox.'


import datetime
import logging
import os
import shutil

import emailer
import settings
import v2dropbox


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

mail = emailer.SendEmail()
config = settings.get_settings('settings.json')
recipients = settings.get_settings('recipients.json')


def email_generator(sub, msg, jcode):
    mail.send_mail(subject=sub, message=msg, journal=jcode)


def for_upload():
    """Returns a dictionary like this
    {u'correction': u'artl23-06_061517_test.zip',
    u'final': u'artl23-04_061517_test.zip',
    u'proof': u'artl23-05_061517_test.zip'}
    """
    folders = {}
    for items in config["input_location"]:
        folders.update({items: config["input_location"][items]})
    for_upload_items = {}
    for key, value in folders.iteritems():
        for files in os.listdir(value):
            for_upload_items.update({key: files})
    return folders, for_upload_items


def process():
    """Get the configurations, input folder and upload to Dropbox. """
    if config:
        folders, for_upload_items = for_upload()
        if for_upload_items:
            for stage, folder in folders.iteritems():
                try:
                    files = for_upload_items[stage]
                    print(stage, '%s%s' % (folder, files))
                    #  Upload to Dropbox
                    logger.info('Processing uploading of %s to Dropbox for %s' % (stage, files))
                    source_path = '%s%s' % (folder, files)
                    journal_title = files[:4].upper()
                    production_editor = recipients['recipients'][journal_title]['name']
                    destination_name = '/SPi/From SPi/%s/%s' % (journal_title, files)
                    dropbox_url = v2dropbox.main(config['dropbox']['access_token'], source_path, destination_name)
                    success_path = '%s%s/%s' % (config['success'], stage, files)
                    #  dropbox_url = "https://www.dropbox.com/s/4crjov2yatqgche"
                    if dropbox_url:
                        print(dropbox_url)
                        shutil.move(source_path, success_path)
                        #  Create success log
                        a = open(config['log']['success'] + str(datetime.date.today()) + '.csv', 'a')
                        a.write('\n%s,%s,%s,%s ' % (stage, files, dropbox_url, str(datetime.datetime.now().time())))
                        a.close()

                        #  Send email
                        logger.info('Creating notification email for %s' % files)
                        replacements = {'[MIT Production Editor]': production_editor,
                                        '[stage]': stage,
                                        '[Journal Title]': journal_title,
                                        '[ZIP FILENAME]': files,
                                        '[url]': dropbox_url}
                        with open(config['email']['template']) as infile, open(config['email']['template'] + '.temp', 'w') as outfile:
                            for line in infile:
                                for src, target in replacements.iteritems():
                                    line = line.replace(src, target)
                                outfile.write(line)
                        subject = config['email']['subject'][stage]
                        sub = subject.replace('[Journal Title]', journal_title)
                        message = open(config['email']['template'] + '.temp').read()
                        try:
                            email_generator(sub, message, journal_title)
                            logger.info('Email sent for %s' % files)
                        except Exception as msg:
                            print(msg)
                        finally:
                            os.remove(config['email']['template'] + '.temp')
                    #  Create failed log
                    a = open(config['log']['failed'] + str(datetime.date.today()) + '.csv', 'a')
                    a.write('\n%s,%s,%s ' % (stage, files, str(datetime.datetime.now().time())))
                    a.close()
                except:
                    pass
        else:
            print('No files found for upload...waiting...')
    else:
        print('No configuration file found..')


if __name__ == '__main__':
    process()
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import dropbox
import re


class TransferData:
    def __init__(self, access_token):
        self.access_token = access_token

    def upload_file(self, file_from, file_to):
        """upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(self.access_token)

        with open(file_from, 'rb') as f:
            dbx.files_upload(f.read(), file_to)
            link = dbx.sharing_create_shared_link(file_to)
            url = link.url
            dl_url = re.sub(r"\?dl\=0", "?dl=1", url)
            return dl_url


def main(access_token, file_from, file_to):
    transferData = TransferData(access_token)
    url = transferData.upload_file(file_from, file_to)
    return url


if __name__ == '__main__':
    access_token = 'DW28V2Iut7AAAAAAAAABwP1JJkP9F46URtO66zn-pu5Tzdx1nz6pRszll6UyQ-Bd'
    file_from = r'D:\Programs\MITDropboxUploader\in\proof\artl23-04_061517_test.zip'
    file_to = '/SPi/From SPi/ARTL/artl23-04_061517_test.zip'  # The full path to upload the file to, including the file name
    main(access_token, file_from, file_to)
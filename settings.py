import json
import os


def get_settings(j_file):
    """Get program settings from a json file."""
    if os.path.isfile(os.getcwd() + '/' + j_file):
        json_settings = open(os.getcwd() + '/' + j_file)
        json_content = json_settings.read()
        settings_content = json.loads(json_content)
        return settings_content
    else:
        print('No settings.json file found')


if __name__ == '__main__':
    get_settings('settings.json')